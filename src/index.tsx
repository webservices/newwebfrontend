import React from "react";
import ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import { App } from '@app/index';
import configureStore from "@app/Store";

if (process.env.NODE_ENV !== "production") {
  // tslint:disable-next-line
  const axe = require("react-axe");
  axe(React, ReactDOM, 1000);
}

const store = configureStore()

const Root = () => {
  return (
    <Provider store={store}>
      <App/>
    </Provider>
  )
}


ReactDOM.render(<Root />, document.getElementById("root") as HTMLElement);
