import * as React from "react";
import {
  PageSection,
  Card,
  Grid,
  CardHeader,
  CardBody,
  Form,
  FormGroup,
  TextInput,
  ActionGroup,
  Button
} from "@patternfly/react-core";
import {
  TableBody,
  Table,
  TableHeader,
  textCenter
} from "@patternfly/react-table";
import { getRedirections } from "@app/WebRedirector/Actions/GetRedirections/GetRedirections";
import { connect } from "react-redux";
import { createRedirection } from "@app/WebRedirector/Actions/CreateRedirection";
import { deleteRedirection } from "@app/WebRedirector/Actions/DeleteRedirection";

const Dashboard: React.FunctionComponent<any> = props => {
  const {
    redirections,
    createRedirection,
    getRedirections,
    deleteRedirection
  } = props;

  const [columns] = React.useState([
    {
      title: "Alias",
      transforms: [textCenter],
      cellTransforms: [textCenter]
    },
    {
      title: "Redirect URL",
      transforms: [textCenter],
      cellTransforms: [textCenter]
    },
    {
      title: "Owner",
      transforms: [textCenter],
      cellTransforms: [textCenter]
    }
  ]);

  const [redirection, setRedirection] = React.useState({
    Alias: "",
    Redirecturl: ""
  });

  const actions = [
    {
      title: "Delete",
      onClick: (event, rowId, rowData, extra) => {
        const redirectionToDelete = redirections.find(r => {
          return r.Alias == rowData.alias.title;
        });
        deleteRedirection(redirectionToDelete.ID);
      }
    }
  ];
  React.useEffect(() => {
    getRedirections();
  }, []);

  const rows = redirections.map(r => [r.Alias, r.Redirecturl, r.Owner]);

  const onSelect = (event, isSelected, rowId) => {
    let rows;
    if (rowId === -1) {
      rows = rows.map(oneRow => {
        oneRow.selected = isSelected;
        return oneRow;
      });
    } else {
      rows = [...rows];
      rows[rowId].selected = isSelected;
    }
  };

  return (
    <PageSection
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around"
      }}
    >
      <div style={{ width: "45%" }}>
        <Card >
          <CardHeader>Create new redirectrion</CardHeader>
          <CardBody>
            <Form>
              <FormGroup
                label="Alias"
                isRequired
                fieldId="crate-redirection-alias"
              >
                <div style={{ display: "flex", flexDirection: "row" }}>
                  <TextInput
                    isRequired
                    type="text"
                    id="crate-redirection-alias"
                    name="crate-redirection-alias"
                    value={redirection.Alias}
                    onChange={Alias =>
                      setRedirection({ ...redirection, Alias })
                    }
                  />
                  <Button
                    variant="primary"
                    onClick={() =>
                      setRedirection({
                        ...redirection,
                        Alias: Math.random()
                          .toString(36)
                          .substring(2, 8)
                      })
                    }
                    style={{
                      marginLeft: "10px"
                    }}
                  >
                    Generate
                  </Button>
                </div>
              </FormGroup>
              <FormGroup
                label="Redirect URL"
                isRequired
                fieldId="crate-redirection-redirecturl"
              >
                <TextInput
                  isRequired
                  type="text"
                  id="crate-redirection-redirecturl"
                  name="crate-redirection-redirecturl"
                  value={redirection.Redirecturl}
                  onChange={Redirecturl =>
                    setRedirection({ ...redirection, Redirecturl })
                  }
                />
              </FormGroup>
              <ActionGroup>
                <Button
                  variant="primary"
                  onClick={() => createRedirection(redirection)}
                >
                  Create
                </Button>
                <Button variant="secondary">Cancel</Button>
              </ActionGroup>
            </Form>
          </CardBody>
        </Card>
      </div>

      <Card style={{ width: "45%" }}>
        <CardHeader>Your redirections</CardHeader>
        <CardBody>
          <Table
            actions={actions}
            onSelect={onSelect}
            cells={columns}
            rows={rows}
          >
            <TableHeader />
            <TableBody />
          </Table>
        </CardBody>
      </Card>
    </PageSection>
  );
};

const mapStateToProps = state => {
  return {
    redirections: state.redirections.redirections
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getRedirections: () => dispatch(getRedirections()),
    createRedirection: redirection => dispatch(createRedirection(redirection)),
    deleteRedirection: redirectionId =>
      dispatch(deleteRedirection(redirectionId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);
