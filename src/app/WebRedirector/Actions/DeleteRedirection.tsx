import { AnyAction } from "redux";
import { ThunkAction, ThunkDispatch } from "redux-thunk";
import axios from "axios";

export const DELETE_REDIRECTION = "DELETE_REDIRECTION";
export const DELETE_REDIRECTION_SUCCESS = "DELETE_REDIRECTION_SUCCESS";
export const DELETE_REDIRECTION_FAILURE = "DELETE_REDIRECTION_FAILURE";

function deleteRedirectionAction() {
  return {
    type: DELETE_REDIRECTION
  };
}

function deleteRedirectionSuccessAction(redirection) {
  return {
    type: DELETE_REDIRECTION_SUCCESS,
    payload: redirection
  };
}

function deleteRedirectionFailureAction(error) {
  return {
    type: DELETE_REDIRECTION_FAILURE,
    payload: error
  };
}

export const deleteRedirection = (
  redirectionId
): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
  return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<any> => {
    dispatch(deleteRedirectionAction());
    try {
      const response = await axios.delete(
        `${process.env.BASE_URL}/redirections/${redirectionId}`
      );
      dispatch(deleteRedirectionSuccessAction(redirectionId));
    } catch (error) {
      dispatch(deleteRedirectionFailureAction(error));
    }
  };
};
