import { AnyAction } from "redux";
import { ThunkAction, ThunkDispatch } from "redux-thunk";
import axios from "axios";
import {
  getRedirectionsAction,
  getRedirectionsSuccessAction,
  getRedirectionsFailureAction
} from "./GetRedirectionsActionCreators";

export const getRedirections = (): ThunkAction<
  Promise<void>,
  {},
  {},
  AnyAction
> => {
  return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<any> => {
    dispatch(getRedirectionsAction());
    try {
      const response = await axios.get(
        process.env.BASE_URL + "/redirections");
      dispatch(getRedirectionsSuccessAction(response.data));
    } catch (error) {
      dispatch(getRedirectionsFailureAction(error));
    }
  };
};
