import Redirection from "@app/WebRedirector/Reducers/Redirection";

export const GET_REDIRECTIONS = "GET_REDIRECTIONS";
export const GET_REDIRECTIONS_SUCCESS = "GET_REDIRECTIONS_SUCCESS";
export const GET_REDIRECTIONS_FAILURE = "GET_REDIRECTIONS_FAILURE";

interface GetRedirections {
    type: typeof GET_REDIRECTIONS
}

interface GetRedirectionsSuccess {
    type: typeof GET_REDIRECTIONS_SUCCESS
    payload: Redirection[]
}

interface GetRedirectionsFailure {
    type: typeof GET_REDIRECTIONS_FAILURE
    payload: Error
}

export type RedirectionActionTypes = GetRedirections | GetRedirectionsSuccess | GetRedirectionsFailure
