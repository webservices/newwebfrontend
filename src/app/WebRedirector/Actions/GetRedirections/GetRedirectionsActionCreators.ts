import { RedirectionActionTypes, GET_REDIRECTIONS, GET_REDIRECTIONS_SUCCESS, GET_REDIRECTIONS_FAILURE } from "./GetRedirectionsTypes"; 
import Redirection from "../../Reducers/Redirection";

export function getRedirectionsAction(): RedirectionActionTypes {
    return { type: GET_REDIRECTIONS }
}

export function getRedirectionsSuccessAction(redirections: Redirection[]): RedirectionActionTypes {
    return {
        type: GET_REDIRECTIONS_SUCCESS,
        payload: redirections
    }
}
export function getRedirectionsFailureAction(error: Error): RedirectionActionTypes {
    return {
        type: GET_REDIRECTIONS_FAILURE,
        payload: error
    }
}
