import { AnyAction } from "redux";
import { ThunkAction, ThunkDispatch } from "redux-thunk";
import axios from "axios";

export const CREATE_REDIRECTION = "CREATE_REDIRECTION";
export const CREATE_REDIRECTION_SUCCESS = "CREATE_REDIRECTION_SUCCESS";
export const CREATE_REDIRECTION_FAILURE = "CREATE_REDIRECTION_FAILURE";

function createRedirectionAction() {
  return {
    type: CREATE_REDIRECTION
  };
}

function createRedirectionSuccessAction(redirection) {
  return {
    type: CREATE_REDIRECTION_SUCCESS,
    payload: redirection
  };
}

function createRedirectionFailureAction(error) {
  return {
    type: CREATE_REDIRECTION_FAILURE,
    payload: error
  };
}

export const createRedirection = (
  redirection
): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
  return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<any> => {
    dispatch(createRedirectionAction());
    try {
      const response = await axios.post(
        process.env.BASE_URL + "/redirections",
        { ...redirection }
      );
      dispatch(createRedirectionSuccessAction(response.data));
    } catch (error) {
      dispatch(createRedirectionFailureAction(error));
    }
  };
};
