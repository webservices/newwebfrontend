export default class Redirection {
    constructor(
        public id: number,
        public alias: string,
        public redirectionurl: string,
        public owner: string
    ) { }
}