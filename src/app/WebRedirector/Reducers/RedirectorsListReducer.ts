import {
  GET_REDIRECTIONS,
  GET_REDIRECTIONS_SUCCESS,
  GET_REDIRECTIONS_FAILURE
} from "../Actions/GetRedirections/GetRedirectionsTypes";
import {
  CREATE_REDIRECTION,
  CREATE_REDIRECTION_SUCCESS,
  CREATE_REDIRECTION_FAILURE
} from "../Actions/CreateRedirection";
import {
  DELETE_REDIRECTION,
  DELETE_REDIRECTION_SUCCESS,
  DELETE_REDIRECTION_FAILURE
} from "../Actions/DeleteRedirection";

const initialState = {
  redirections: [],
  loading: false,
  error: undefined
};

const processGetRedirections = state => {
  return {
    ...state,
    loading: true
  };
};

const processGetRedirectionsSuccess = (state, redirections) => {
  return {
    ...state,
    redirections,
    loading: false
  };
};

const processGetRedirectionsFailure = (state, error) => {
  return {
    ...state,
    redirections: [],
    error,
    loading: false
  };
};

const processCreateRedirection = state => {
  return {
    ...state,
    loading: true
  };
};

const processCreateRedirectonSuccess = (state, redirection) => {
  return {
    ...state,
    loading: false,
    redirections: [...state.redirections, redirection]
  };
};

const processCreateRedirectionFailure = (state, error) => {
  return {
    ...state,
    loading: false,
    error
  };
};

const processDeleteRedirection = state => {
  return {
    ...state,
    loading: true
  };
};

const processDeleteRedirectionSucess = (state, redirectionId) => {
  return {
    ...state,
    loading: false,
    redirections: state.redirections.filter(r => r.ID !== redirectionId)
  };
};

const processDeleteRedirectionFailure = (state, error) => {
  return {
    ...state,
    loading: false,
    error
  };
};

export function redirectionsReducer(state = initialState, action) {
  switch (action.type) {
    case GET_REDIRECTIONS:
      return processGetRedirections(state);
    case GET_REDIRECTIONS_SUCCESS:
      return processGetRedirectionsSuccess(state, action.payload);
    case GET_REDIRECTIONS_FAILURE:
      return processGetRedirectionsFailure(state, action.payload);

    case CREATE_REDIRECTION:
      return processCreateRedirection(state);
    case CREATE_REDIRECTION_SUCCESS:
      return processCreateRedirectonSuccess(state, action.payload);
    case CREATE_REDIRECTION_FAILURE:
      return processCreateRedirectionFailure(state, action.payload);

    case DELETE_REDIRECTION:
      return processDeleteRedirection(state);
    case DELETE_REDIRECTION_SUCCESS:
      return processDeleteRedirectionSucess(state, action.payload);
    case DELETE_REDIRECTION_FAILURE:
      return processDeleteRedirectionFailure(state, action.payload);

    default:
      return state;
  }
}
