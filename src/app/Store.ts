import {applyMiddleware, combineReducers, createStore, compose, Store} from 'redux'
import thunk from 'redux-thunk';
import {
    redirectionsReducer,
  } from './WebRedirector/Reducers/RedirectorsListReducer';
  

const rootReducer = combineReducers({
    redirections: redirectionsReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


export default function configureStore() {
    const store = createStore(rootReducer, undefined, composeEnhancers(applyMiddleware(thunk)))
    return store
}